var nbImg = 29;

$(document).ready(function () {
  console.log("ready!");
  var imgHeaderWrapper = $("#random-img-title");
  var imgs = imgHeaderWrapper.find("img");
  var hueCount = 0;
  var count = 1;

  var i = setInterval(function () {
    hueCount += Math.floor(360 / imgs.length);
    imgHeader(imgs, count, hueCount);
    if (count >= imgs.length) {
      count = 0;
    } else {
      count++;
    }
  }, 3000);
  function imgHeader(els, i, hueCount) {
  // console.log("imgHeader -> els", els[i])
    // $(els[i]).css({
    //   opacity: 1,
    // })
    els.each(function (index) {
      if(index !== i){
        $(this).css({
          opacity: 0
        })
      } else {
        $(this).css({
          opacity: 1
        })
      }
      $(this).css({ filter: "hue-rotate(" + hueCount + "deg)" });
    })

  }
  /*
  function imgHeader(nbImg, el, hueDeg) {
    var random = Math.floor(Math.random() * nbImg);
    var stringR = String(random).padStart(2, "0");
    // el.attr('src', '/user/themes/ws-theme/images/export-jpg-blue/logo-vecto-' + stringR + '.jpg')
    el.attr("src", "../images/export-jpg-blue/logo-vecto-" + stringR + ".jpg");
    el.css({ filter: "hue-rotate(" + hueDeg + "deg)" });
  }*/
});
